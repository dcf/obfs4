package main

import (
	"fmt"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/yawning/obfs4.git/common/log"
	"gitlab.com/yawning/obfs4.git/transports/base"
)

// stringAddr implements the net.Addr interface by returning static strings.
type stringAddr struct{ network, address string }

func (addr stringAddr) Network() string {
	return addr.network
}

func (addr stringAddr) String() string {
	return addr.address
}

// clientPacketConn implements the net.PacketConn interface atop a reliable
// net.Conn. Whenever the net.Conn is interrupted (Read or Write returns an
// error), clientPacketConn dials the same address again and starts sending
// and receiving packets over the new net.Conn. An error in ReadFrom or WriteTo
// is returned only when there is a problem dialing the address.
type clientPacketConn struct {
	addr      string
	clientID  clientID
	recvQueue chan []byte
	sendQueue chan []byte
	closed    chan struct{}
	closeOnce sync.Once
	// The first dial error, which causes the clientPacketConn to be
	// closed and is returned from future read/write operations. Compare to
	// the rerr and werr in io.Pipe.
	err atomic.Value
}

func newClientPacketConn(f base.ClientFactory, addr string, dialFn base.DialFunc, args interface{}) (*clientPacketConn, error) {
	c := &clientPacketConn{
		addr:      addr,
		clientID:  newClientID(),
		recvQueue: make(chan []byte, 32),
		sendQueue: make(chan []byte, 32),
		closed:    make(chan struct{}),
		err:       atomic.Value{},
	}
	go c.dialLoop(f, dialFn, args)
	return c, nil
}

func (c *clientPacketConn) dialLoop(f base.ClientFactory, dialFn base.DialFunc, args interface{}) {
	var err error
	for err == nil {
		select {
		case <-c.closed:
			return
		default:
		}
		err = c.dialAndExchange(f, dialFn, args)
	}
	c.closeWithError(err)
}

func (c *clientPacketConn) dialAndExchange(f base.ClientFactory, dialFn base.DialFunc, args interface{}) error {
	name := f.Transport().Name()
	addrStr := log.ElideAddr(c.addr)

	conn, err := f.Dial("tcp", c.addr, dialFn, args)
	// A dial error is a real error to be reported to upper layers.
	if err != nil {
		return err
	}
	defer conn.Close()
	log.Infof("%s(%s) - new outer connection", name, addrStr)

	readErrCh := make(chan error)
	writeErrCh := make(chan error)
	go func() {
		defer close(readErrCh)
		for {
			_, p, err := readPacket(conn)
			if err != nil {
				readErrCh <- err
				return
			}
			select {
			case <-c.closed:
				return
			case c.recvQueue <- p:
			}
		}
	}()
	go func() {
		defer close(writeErrCh)
		for {
			select {
			case <-c.closed:
				return
			case p := <-c.sendQueue:
				err := writePacket(conn, c.clientID, p)
				if err != nil {
					writeErrCh <- err
					return
				}
			}
		}
	}()

	readErr := <-readErrCh
	writeErr := <-writeErrCh
	err = readErr
	if err == io.EOF {
		err = nil
	}
	if err == nil {
		err = writeErr
	}
	if err != nil {
		log.Warnf("%s(%s) - closed outer connection: %s", name, addrStr, log.ElideError(err))
	} else {
		log.Infof("%s(%s) - closed outer connection", name, addrStr)
	}

	return nil
}

func (c *clientPacketConn) ReadFrom(p []byte) (int, net.Addr, error) {
	select {
	case <-c.closed:
		return 0, nil, &net.OpError{Op: "read", Net: c.LocalAddr().Network(), Addr: c.LocalAddr(), Err: c.err.Load().(error)}
	case buf := <-c.recvQueue:
		return copy(p, buf), stringAddr{"tcp", c.addr}, nil
	}
}

func (c *clientPacketConn) WriteTo(p []byte, addr net.Addr) (int, error) {
	// Ignore addr.
	buf := make([]byte, len(p))
	copy(buf, p)
	select {
	case <-c.closed:
		return 0, &net.OpError{Op: "write", Net: c.LocalAddr().Network(), Addr: addr, Source: c.LocalAddr(), Err: c.err.Load().(error)}
	case c.sendQueue <- buf:
		return len(buf), nil
	}
}

func (c *clientPacketConn) closeWithError(err error) error {
	var once bool
	c.closeOnce.Do(func() {
		// Store the error to be returned by future read/write
		// operations.
		if err == nil {
			err = fmt.Errorf("operation on closed connection")
		}
		c.err.Store(err)
		close(c.closed)
		once = true
	})
	if !once {
		return &net.OpError{Op: "close", Net: c.LocalAddr().Network(), Addr: c.LocalAddr(), Err: c.err.Load().(error)}
	}
	return nil
}

func (c *clientPacketConn) Close() error {
	return c.closeWithError(nil)
}

func (c *clientPacketConn) LocalAddr() net.Addr {
	// Return a fixed placeholder address, because the local address of the
	// temporary net.Conns we dial will vary--but it's the same notional
	// net.PacketConn.
	return stringAddr{"local", "local"}
}

func (c *clientPacketConn) SetDeadline(t time.Time) error {
	var err error
	if errRead := c.SetReadDeadline(t); errRead != nil {
		err = errRead
	}
	if errWrite := c.SetWriteDeadline(t); errWrite != nil {
		err = errWrite
	}
	return err
}

func (c *clientPacketConn) SetReadDeadline(t time.Time) error {
	return fmt.Errorf("not implemented")
}

func (c *clientPacketConn) SetWriteDeadline(t time.Time) error {
	return fmt.Errorf("not implemented")
}
