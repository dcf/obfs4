package main

import (
	"bytes"
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"io"
)

// clientID implements the net.Addr interface around a 64-bit client ID. The
// client ID is an additional connection identifier layered on top of QUIC
// packets, because quic-go does not expose the QUIC connection ID.
type clientID uint64

func newClientID() clientID {
	var client uint64
	err := binary.Read(rand.Reader, binary.BigEndian, &client)
	if err != nil {
		panic(err)
	}
	return clientID(client)
}

func (addr clientID) Network() string {
	return "clientid"
}

func (addr clientID) String() string {
	return fmt.Sprintf("%08x", uint32(addr))
}

// Read an entire ID- and length-prefixed packet from r. err == nil if and only
// if the entirety was received. err == io.EOF only if the io.Reader ended after
// 0 bytes.
func readPacket(r io.Reader) (clientID, []byte, error) {
	var client uint64
	err := binary.Read(r, binary.BigEndian, &client)
	if err != nil {
		return 0, nil, err
	}
	var length uint16
	err = binary.Read(r, binary.BigEndian, &length)
	if err == io.EOF {
		// The io.Reader hit EOF in the middle of a larger structure.
		// For us, that's not a normal EOF, it's an anomalous EOF.
		err = io.ErrUnexpectedEOF
	}
	if err != nil {
		return 0, nil, err
	}
	p := make([]byte, int(length))
	n, err := io.ReadFull(r, p)
	if err == io.EOF {
		err = io.ErrUnexpectedEOF
	}
	return clientID(client), p[:n], err
}

// Write an entire ID- and length-prefixed packet to w. err == nil only if the
// entirety was written successfully.
func writePacket(w io.Writer, client clientID, p []byte) error {
	length := uint16(len(p))
	if int(length) != len(p) {
		return fmt.Errorf("packet too large")
	}
	var buf bytes.Buffer
	binary.Write(&buf, binary.BigEndian, uint64(client))
	binary.Write(&buf, binary.BigEndian, length)
	buf.Write(p)
	_, err := w.Write(buf.Bytes())
	if err != nil {
		return err
	}
	return err
}
