package main

import (
	"container/heap"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"math/big"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"git.torproject.org/pluggable-transports/goptlib.git"
	"gitlab.com/yawning/obfs4.git/common/log"
	"gitlab.com/yawning/obfs4.git/transports/base"
)

// taggedPacket is a packet tagged with an address (encapsulating the return
// type of PacketConn.ReadFrom).
type taggedPacket struct {
	P    []byte
	Addr net.Addr
}

// lockConn combines a sync.Mutex and a net.Conn. The Lock method is meant to be
// used by callers of connMap.Get, to enforce exclusive access to the returned
// net.Conn.
type lockConn struct {
	sync.Mutex
	net.Conn
}

// serverPacketConn simulates a single net.PacketConn over all the incoming
// ordinary network connections on a net.Listener. Incoming streams are decoded
// into discrete packets to be returned from the ReadFrom method. WriteTo
// encodes packets into streams and writes them back out to an ordinary network
// connection. Outgoing packets are written to the network connection on which a
// packet from the corresponding client ID was most recently received.
type serverPacketConn struct {
	ln        net.Listener
	connMap   *connMap
	recvQueue chan taggedPacket
	closed    chan struct{}
	closeOnce sync.Once
	err       atomic.Value
}

func newServerPacketConn(f base.ServerFactory, addr *net.TCPAddr, info *pt.ServerInfo, connMapTimeout time.Duration) (*serverPacketConn, error) {
	ln, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return nil, err
	}
	c := &serverPacketConn{
		ln:        ln,
		connMap:   newConnMap(connMapTimeout),
		recvQueue: make(chan taggedPacket, 32),
		closed:    make(chan struct{}),
		err:       atomic.Value{},
	}
	go c.serverAcceptLoop(f, info)
	return c, nil
}

// Accept incoming connections and pass them to c.handleConn.
func (c *serverPacketConn) serverAcceptLoop(f base.ServerFactory, info *pt.ServerInfo) error {
	defer c.ln.Close()
	for {
		conn, err := c.ln.Accept()
		if err != nil {
			if err, ok := err.(*net.OpError); ok && err.Temporary() {
				continue
			}
			c.closeWithError(err)
			return err
		}
		go c.serverHandler(f, conn, info)
	}
}

// Read packets and feed them into c.recvQueue, to later be returned from
// c.ReadFrom. This function never writes to conn; that only happens in
// c.WriteTo, which has the necessary context to know which net.Conn currently
// corresponds to a given client ID.
func (c *serverPacketConn) serverHandler(f base.ServerFactory, conn net.Conn, info *pt.ServerInfo) {
	defer conn.Close()

	name := f.Transport().Name()
	addrStr := log.ElideAddr(conn.RemoteAddr().String())
	log.Infof("%s(%s) - new outer connection", name, addrStr)
	defer log.Infof("%s(%s) - closed outer connection", name, addrStr)

	// Instantiate the server transport method and handshake.
	remote, err := f.WrapConn(conn)
	if err != nil {
		log.Warnf("%s(%s) - handshake failed: %s", name, addrStr, log.ElideError(err))
		return
	}

	downstream := &lockConn{Conn: remote}

	for {
		client, p, err := readPacket(remote)
		if err == io.EOF {
			return
		} else if err != nil {
			log.Errorf("%s(%s) - reading packet: %s", name, addrStr, log.ElideError(err))
			return
		}
		// Mark this net.Conn as the one on which WriteTo should send
		// downstream data associated with this client ID.
		c.connMap.Update(client, downstream)
		select {
		case <-c.closed:
			return
		case c.recvQueue <- taggedPacket{p, client}:
		default: // Okay to drop packets.
		}
	}
}

func (c *serverPacketConn) ReadFrom(p []byte) (int, net.Addr, error) {
	select {
	case <-c.closed:
		return 0, nil, &net.OpError{Op: "read", Net: c.LocalAddr().Network(), Addr: c.LocalAddr(), Err: c.err.Load().(error)}
	case tp := <-c.recvQueue:
		return copy(p, tp.P), tp.Addr, nil
	}
}

func (c *serverPacketConn) WriteTo(p []byte, addr net.Addr) (int, error) {
	select {
	case <-c.closed:
		return 0, &net.OpError{Op: "write", Net: addr.Network(), Addr: addr, Source: c.LocalAddr(), Err: c.err.Load().(error)}
	default:
	}
	// Find the net.Conn on which a packet with this client ID was
	// most recently received.
	conn := c.connMap.Get(addr).(*lockConn)
	if conn == nil {
		// This shouldn't ordinarily happen, as the connMigrationMap
		// stores a client ID as long as a quic.Session is alive. It may
		// happen if there is a client ID collision, in which case the
		// connection is useless anyway.
		return 0, &net.OpError{Op: "write", Net: addr.Network(), Addr: addr, Source: c.LocalAddr(), Err: fmt.Errorf("no mapped net.Conn")}
	}
	conn.Lock()
	_ = writePacket(conn, addr.(clientID), p) // Ignore error.
	conn.Unlock()
	// Always return success -- even if the net.Conn we have is returning
	// errors, we expect to be able to succeed later on another one.
	return len(p), nil
}

func (c *serverPacketConn) closeWithError(err error) error {
	var once bool
	c.closeOnce.Do(func() {
		// Store the error to be returned by future read/write
		// operations.
		if err == nil {
			err = fmt.Errorf("operation on closed connection")
		}
		c.err.Store(err)
		close(c.closed)
		once = true
	})
	if !once {
		return &net.OpError{Op: "close", Net: c.LocalAddr().Network(), Addr: c.LocalAddr(), Err: c.err.Load().(error)}
	}
	return nil
}

func (c *serverPacketConn) Close() error {
	return c.closeWithError(nil)
}

func (c *serverPacketConn) LocalAddr() net.Addr {
	return c.ln.Addr()
}

func (c *serverPacketConn) SetDeadline(t time.Time) error {
	var err error
	if errRead := c.SetReadDeadline(t); errRead != nil {
		err = errRead
	}
	if errWrite := c.SetWriteDeadline(t); errWrite != nil {
		err = errWrite
	}
	return err
}

func (c *serverPacketConn) SetReadDeadline(t time.Time) error {
	return fmt.Errorf("not implemented")
}

func (c *serverPacketConn) SetWriteDeadline(t time.Time) error {
	return fmt.Errorf("not implemented")
}

// Remove addr from the internal connection migration tracking map.
func (c *serverPacketConn) Forget(addr net.Addr) {
	c.connMap.Delete(addr)
}

// connMapRecord is a single client→net.Conn mapping, along with the time it was
// last seen.
type connMapRecord struct {
	Addr     net.Addr
	Conn     net.Conn
	LastSeen time.Time
}

// connMap manages a set of client addresses (randomly generated client IDs) and
// maps them to the net.Conn on which a packet from them was most recently
// received. This is meant to provide connection migration for a QUIC session: if
// we start receiving upstream packets on a new net.Conn, we'll start sending
// downstream packets on that same net.Conn. connMap is safe to use from
// multiple goroutines.
type connMap struct {
	// We use an inner structure to avoid exposing public heap.Interface
	// functions to users of connMap.
	inner connMap_
	// Synchronizes access to inner.
	lock sync.Mutex
}

// Create a new connMap that expires clients after a timeout.
func newConnMap(timeout time.Duration) *connMap {
	m := &connMap{
		inner: connMap_{
			byAge:  make([]*connMapRecord, 0),
			byAddr: make(map[net.Addr]int),
		},
	}
	go func() {
		for {
			time.Sleep(timeout / 2)
			now := time.Now()
			m.lock.Lock()
			m.inner.removeExpired(now, timeout)
			m.lock.Unlock()
		}
	}()
	return m
}

// Return the net.Conn that addr maps to, or nil if there is no mapping.
func (m *connMap) Get(addr net.Addr) net.Conn {
	m.lock.Lock()
	defer m.lock.Unlock()
	return m.inner.Get(addr, time.Now())
}

// Update the mapping for addr to point to conn, and touch the LastSeen
// timestamp.
func (m *connMap) Update(addr net.Addr, conn net.Conn) {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.inner.Update(addr, conn, time.Now())
}

// Remove any mapping for addr.
func (m *connMap) Delete(addr net.Addr) {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.inner.Delete(addr)
}

// The inner type of connMap, that implements heap.Interface. byAge is the
// backing store, a heap ordered by last-seen time, which facilitates expiring
// old client records. byAddr is a map from addresses to heap indices, to allow
// looking up by address. Unlike connMap, this structure requires external
// synchonization.
type connMap_ struct {
	byAge  []*connMapRecord
	byAddr map[net.Addr]int
}

// heap.Interface for connMap.

func (inner *connMap_) Len() int {
	if len(inner.byAge) != len(inner.byAddr) {
		panic("inconsistent connMap")
	}
	return len(inner.byAge)
}

func (inner *connMap_) Less(i, j int) bool {
	return inner.byAge[i].LastSeen.Before(inner.byAge[j].LastSeen)
}

func (inner *connMap_) Swap(i, j int) {
	inner.byAge[i], inner.byAge[j] = inner.byAge[j], inner.byAge[i]
	inner.byAddr[inner.byAge[i].Addr] = i
	inner.byAddr[inner.byAge[j].Addr] = j
}

func (inner *connMap_) Push(x interface{}) {
	record := x.(*connMapRecord)
	if _, ok := inner.byAddr[record.Addr]; ok {
		panic("duplicate address in connMap")
	}
	// Insert into byAddr map.
	inner.byAddr[record.Addr] = len(inner.byAge)
	// Insert into byAge slice.
	inner.byAge = append(inner.byAge, record)
}

func (inner *connMap_) Pop() interface{} {
	n := len(inner.byAddr)
	// Remove from byAge slice.
	record := inner.byAge[n-1]
	inner.byAge[n-1] = nil
	inner.byAge = inner.byAge[:n-1]
	// Remove from byAddr map.
	delete(inner.byAddr, record.Addr)
	return record
}

// Remove all records whose LastSeen timestamp is more than timeout in the past.
func (inner *connMap_) removeExpired(now time.Time, timeout time.Duration) {
	for len(inner.byAge) > 0 && now.Sub(inner.byAge[0].LastSeen) >= timeout {
		heap.Pop(inner)
	}
}

func (inner *connMap_) Get(addr net.Addr, now time.Time) net.Conn {
	if i, ok := inner.byAddr[addr]; ok {
		record := inner.byAge[i]
		record.LastSeen = now
		heap.Fix(inner, i)
		return inner.byAge[i].Conn
	} else {
		return nil
	}
}

func (inner *connMap_) Update(addr net.Addr, conn net.Conn, now time.Time) {
	var record *connMapRecord
	if i, ok := inner.byAddr[addr]; ok {
		record = inner.byAge[i]
		record.Conn = conn
		record.LastSeen = now
		heap.Fix(inner, i)
	} else {
		// Not found, create a new one.
		record = &connMapRecord{
			Addr:     addr,
			Conn:     conn,
			LastSeen: now,
		}
		heap.Push(inner, record)
	}
}

func (inner *connMap_) Delete(addr net.Addr) {
	if i, ok := inner.byAddr[addr]; ok {
		heap.Remove(inner, i)
	}
}

func generateTLSCertificate() (*tls.Certificate, error) {
	// https://golang.org/src/crypto/tls/generate_cert.go
	key, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}

	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
	}
	certDER, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
	if err != nil {
		return nil, err
	}
	keyDER, err := x509.MarshalECPrivateKey(key)
	if err != nil {
		return nil, err
	}

	cert, err := tls.X509KeyPair(
		pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: certDER}),
		pem.EncodeToMemory(&pem.Block{Type: "EC PRIVATE KEY", Bytes: keyDER}),
	)
	return &cert, err
}
